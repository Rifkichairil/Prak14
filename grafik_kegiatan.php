<?php
    include_once 'top_php';

    require_once 'db/class_kegiatan.php';

    $obj = new Kegiatan(); //buat instal kelas objek
    $rs = $obj->getStatistik(); //paggil method statistik
    $ar_data = []; //siapkan array kosong

    foreach ($rs as $row) {
        $ar['label']=$row['nama']; //buat array key label
        $ar['y']=(int)$row['jumlah']; //buat array key y
        $ar_data[]=$ar; // masukan array ke ar_data
    }
    $out = array_values($ar_data); // ubah format menjeadi array_values
    //masukkan template top
    //import class DML

    //buat objek untuk memanggil class kegiatan

    //panggil fungsi untuk mendapat statistik

    //persiapkan array untuk menampung data

    //looping

        //array menyimpan nama sumbu berisi colum dalam query DML

        //simpan dalam array

    //buat variabel untuk menyimpan hasil array

?>
<!--buat javascript untuk menampilkan data-->
<script type="text/javascript">
    window.onload = function (){

var chart = new CanvasJS.Chart("chartCountainer", {
    theme : "light1", // "light2", "dark1", "dark2"
    animationEnable: false, //change to true
   
    title:{
            text:"Basic Colomn Chart"
    },
    
    data: [
    {
            // Change type to "bar", "area", "spline", "pie",etc.
            type: "column",
             dataPoints:<?php echo json_encode($out) ?>
            // [
                 //   { label: "Conferences",   y: 0 },
                  //  { label: "Kuliah Umum",   y: 1 },
                   // { label: "Seminar",       y: 3 },
                    //{ label: "Training",      y: 0 },
                    //{ label: "Workshop",      y: 0 }
            //]
    }
    ]
});
    chart.render();
    }
    //buat fungsi dimana akan jalan saat page dibuka

        //buat variabel untuk membuat chart
        //parameter->nama id, tema, animasi, judul, data
        //nama id
        //nama theme
        //animasi
        //judul
            //nama judul

        //tempat data
            //tipe grafik
            //isi data

        //render grafiknya

</script>
</head>

<body>
    <div id="chartCountainer" style="height: 370px; width: 100%;"></div><script src="https://canvasjs.com/assets/script/canvasjs.min.js">
    </script>
    <!--Tempat menampilkan chart-->
<!--panggil file jquery untuk grafik-->
<?php
    //masukkan template bottom
    include_once 'bottom.php';
?>