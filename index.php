<?php
include_once 'top.php';
require_once 'db/class_kegiatan.php';
?>
<h2>Daftar Kegiatan</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kegiatan.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kegiatan
    </a>
</div>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->

<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>No</th><th>Kode</th><th>Judul</th><th>Narasumber</th><th>Deskripsi</th><th>Kategori</th><th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['kode'].'</td>';
        echo '<td>'.$row['judul'].'</td>';
        echo '<td>'.$row['narasumber'].'</td>';
        echo '<td>'.$row['deskripsi'].'</td>';
        echo '<td>'.$row['kategori_id'].'</td>';
        echo '<td><a href="view_kegiatan.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kegiatan.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>